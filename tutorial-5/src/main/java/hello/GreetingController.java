package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                   String name, Model model) {
        model.addAttribute("name", name);{
            if (name == null || name.equals("")) {
                model.addAttribute("title", "This is my CV");
            } else {
                model.addAttribute("title", name + ", I hope you are interested to hire me");
            }

            StringBuilder cv = new StringBuilder();
            cv.append("Name: Rafli Hidayat\n");
            cv.append("Birthdate: 19/01/1999\n");
            cv.append("Birthplace: Surabaya\n");
            cv.append("Address: Belakang Citos\n");
            cv.append("Education History:\n");
            cv.append("- SMP Labschool Kebayoran 2011-2013\n");
            cv.append("- SMA 34 Jakarta 2014-2016\n");
            cv.append("- Faculty of Computer Science 2016-Present\n");
            model.addAttribute("cv", cv.toString());

            StringBuilder description = new StringBuilder();
            description.append("I am a second-year CS student at Universitas Indonesia. ");
            description.append("Previously, I've interned at Mandiri Sekuritas as UI/UX Designer. ");
            description.append("Currently, I'm a Vice Project Officer of The 25th Computers Get Together. ");
            description.append("Interests in photography.");
            model.addAttribute("description", description.toString());

            return "greeting";
        }

    }}